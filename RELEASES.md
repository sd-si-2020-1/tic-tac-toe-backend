# RELEASES

## [0.3.0 (2020-10-29)](https://gitlab.com/sd-si-2020-1/tic-tac-toe-backend/-/releases/v0.3.0)
  * documentação com PlantUML:
    * instalação da extensão PlantUML no VS Code
    * diagramas de caso de uso
      * exemplos de caso de uso textuais: uc1, uc2, ... uc6
      * exemplo de diagrama de caso de uso: papeis  
      * exemplo de implementação: models/game e models/session
    * diagramas de componentes
      * exemplo: backend_components
    * diagramas de sequência
      * exemplos:
        * uc1_cadastrar_conta_fluxo_normal 
        * uc1_cadastrar_conta_erro_login_existe
        * uc3_abrir_sessao
        * uc6_realizar_movimento
      * exemplo de implementação:
        * uc3: models/session - método open
        * uc6: models/game - método move
  * documentação com jsdoc:
    * instalação do pacote jsdoc-to-markdown no nodejs
    * instalação da extensão markdown preview enhanced no VS Code
    * documentação de classes
    * documentação de métodos
    * documentação de módulos 
    * alteração do package.json para gerar documentação através do comando 'npm run docs'
    * documentação da API do módulo models
    * documentação da API do módulo db
  * biblioteca de funções utilitárias
    * app/utils: funções gerais
    * db/utils: funções para banco de dados

## [0.2.1 (2020-10-27)](https://gitlab.com/sd-si-2020-1/tic-tac-toe-backend/-/releases/v0.2.1)
  * modularização do código:
    * criação de módulos app, db, rest e tests
    * criação dos objetos db/database e app/config
    * criação do módulo rest/index.js, com suporte a iniciar o servidor REST ou documentação Swagger
  * introdução a testes com [Jest](https://jestjs.io/):
    * criação de teste unitário em tests/db/database.test.js
    * alteração do package.json para executar testes através do comando 'npm test'
  * documentação passo-a-passo com [Codetour](https://marketplace.visualstudio.com/items?itemName=vsls-contrib.codetour):
    * apresentação da v.0.2.1 passo a passo em .tours/v0.2.1.tour
  
## [0.2.0 (2020-10-09)](https://gitlab.com/sd-si-2020-1/tic-tac-toe-backend/-/releases/v0.2.0)
* modelo gameSchema para armazenar partidas
reuso do objeto tictactoe, semelhante a herança
* uso de module.exports para reuso de código
melhorias na documentação da API rest utilizando tag para agrupar rotas correlatas (game e tictactoe)


## [0.1.0 (2020-10-02)](https://gitlab.com/sd-si-2020-1/tic-tac-toe-backend/-/releases/0.1.0)
* versão mínima didática
implementação do objeto tictactoe.js com atributos (board, status, next, count e winner) e métodos (move e calculateWinner)
* utilização da biblioteca restify para implementar a interface REST dos métodos board, status, winner e move
* utilização da biblioteca swagger para documentar a interface REST através do padrão OPENAPI
