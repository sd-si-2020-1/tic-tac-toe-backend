# Tic Tac Toe Backend

Tic-tac-toe (jogo da velha) projeto utilizado como tutorial-exemplo para backend

## Requirements
* NodeJS >= 12
* npm > 6.12
* GNU/Linux distribution
* git

## Install

```
git clone https://gitlab.com/sd-si-2020-1/tic-tac-toe-backend
cd tic-tac-toe-backend
npm install
```

## Run

```
cd tic-tac-toe-backend
node app.js start
```

## Test

Open your browser at http://localhost:8080/docs/swagger, read API documentation and execute tests.

## Manuals and Tutorials

* [Manual do desenvolvedor](docs/README.md)