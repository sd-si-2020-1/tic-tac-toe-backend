# Diagramas de sequência

Veja também: [Manual do desenvolvedor](README.md)

## Introdução
Sequence Diagrams
* tutorial: https://nbviewer.jupyter.org/urls/gitlab.com/marceloakira/tutorial/raw/master/diagramas-de-sequencia/diagramas-de-sequencia.ipynb
* plantuml docs: https://plantuml.com/sequence-diagram

## UC1 - Caso de uso 1: Cadastrar conta

### Fluxo normal

```plantuml
@startuml uc1_cadastrar_conta_fluxo_normal
title **UC1 - CADASTRAR CONTA - FLUXO NORMAL**
autonumber
actor Visitante
boundary Frontend
participant Player as PlayerREST <<rest module>>
participant Player as PlayerDB <<db module>>
Visitante -> Frontend : solicita cadastro\nde conta
Frontend -> PlayerREST : POST /player/create \n {"login": <login>, \n"password": <pwd>}
database Database
PlayerREST -> PlayerDB : verifica disponibilidade\nPlayer.findByLogin(<login>)
PlayerDB -> Database : find <login>\nna coleção players
Database -> PlayerDB : resultado <false>
PlayerDB -> PlayerREST: resultado <false>
PlayerREST -> PlayerDB : instancia\np = Player(<login>,<pwd>);\np.save()
PlayerDB -> Database: save <login, pwd>\nna coleção players
Database -> PlayerDB : resultado <true>
PlayerDB -> PlayerREST : resultado <true>
PlayerREST -> Frontend : RESPONSE 200\n{ "status": "sucess" }
Frontend -> Visitante : cadastro realizado\ncom sucesso
@enduml
```

### Erro de login existente

```plantuml
@startuml uc1_cadastrar_conta_erro_login_existe
title **UC1 - CADASTRAR CONTA - ERRO DE LOGIN EXISTENTE**
autonumber
actor Visitante
boundary Frontend
participant Player as PlayerREST <<rest module>>
participant Player as PlayerDB <<db module>>
Visitante -> Frontend : solicita cadastro\nde conta
Frontend -> PlayerREST : POST /player/create \n {"login": <login>, \n"password": <pwd>}
database Database
PlayerREST -> PlayerDB : verifica disponibilidade\nPlayer.findByLogin(<login>)
PlayerDB -> Database : find <login>\nna coleção players
Database -> PlayerDB : resultado <true>
PlayerDB -> PlayerREST: resultado <true>
PlayerREST -> Frontend : RESPONSE 501\n{ "error": <ErrorLoginAlreadyExists> }
Frontend -> Visitante : login já existe, escolha outro
@enduml
```

## UC3 - Caso de uso 3: Abrir sessão

### Fluxo normal

```plantuml
@startuml uc3_abrir_sessao
title **UC3 - ABRIR SESSÃO - FLUXO NORMAL**
autonumber
actor Visitante
boundary Frontend
participant Session as SessionREST <<rest module>>
participant Player as PlayerDB <<db module>>
participant Session as SessionDB <<db module>>
Visitante -> Frontend : efetua login no sistema
Frontend -> SessionREST : POST /session/open \n {"login": <login>, \n"password": <pwd>}
SessionREST -> PlayerDB : uc3.1 - verificar a senha\nPlayer.check_pwd(<login,pwd>)
'uc3.1 - verificar senha
'PlayerDB -> Database : localiza o jogador pelo login\nPlayer.findByLogin(<login>)
'Database -> PlayerDB : resultado:\n<<object>><player>
'PlayerDB -> PlayerDB : verifica a senha\n<player>.check_pwd(<pwd>)
PlayerDB -> SessionREST : resultado:\n<player>
SessionREST -> SessionDB : uc3.2 - abrir sessão \nSession.open(<player>)
'uc3.2 - abrir sessão
'SessionDB -> SessionDB : verifica se já tem sessão aberta\nSession.isOpened(<player>)
'SessionDB -> SessionDB : resultado <false>
'SessionDB -> Database : instancia <session> e salva <session>.save()s
'Database -> SessionDB : resultado ok
SessionDB -> SessionREST : resultado: \n<session>
SessionREST -> Frontend : RESPONSE 200\n{ "auth": true, "token": token }\n<<jwt token>>
Frontend -> Visitante : login realizado\ncom sucesso 
@enduml
```

## UC3.1 - Parte do caso de uso 3: Verificar senha

```plantuml
@startuml uc3.1_verificar_senha
title **UC3.1 - VERIFICAR SENHA - FLUXO NORMAL**
autonumber
participant Any as Any <<any module>>
participant Player as PlayerDB <<db module>>
Any -> PlayerDB : verifica a senha\nPlayer.check_pwd(<login,pwd>)
PlayerDB -> Database : localiza o jogador pelo login\nPlayer.findByLogin(<login>)
Database -> PlayerDB : resultado:\n<player>
PlayerDB -> PlayerDB : verifica a senha\n<player>.check_pwd(<pwd>)
PlayerDB -> Any : resultado ok:\n<player>
@enduml
```

## UC3.2 - Parte do caso de uso 3: Abrir sessão

```plantuml
@startuml uc3.2_abrir_sessão
title **UC3.2 - ABRIR SESSÃO - FLUXO NORMAL**
autonumber
participant Any as Any <<any module>>
participant Session as SessionDB <<db module>>
participant Session as SessionModel <<model module>>
Any -> SessionDB : abrir sessão \nSession.open(<player>)
SessionDB -> SessionModel : abrir sessão\nSession.open(<player>)
SessionModel -> SessionDB : resultado ok: <session>
SessionDB -> Database : salva sessão: <session>.save()
Database -> SessionDB : resultado ok
SessionDB -> Any : resultado ok: \n<session>
@enduml
```

## UC6 - Caso de uso 6: Realizar movimento

### Fluxo normal

```plantuml
@startuml uc6_realizar_movimento
title **UC6 - REALIZAR MOVIMENTO - FLUXO NORMAL**
autonumber
actor Jogador
boundary Frontend
participant Game as GameREST <<rest module>>
participant Game as GameDB <<db module>>
Jogador -> Frontend : reliza movimento
Frontend -> GameREST : GET\n/game/<id>/move/<pos>
database Database
GameREST -> GameDB : obtém game identificado pelo id \nGame.findById(<id>)
GameDB -> Database : find <id>\nna coleção Games
Database -> GameDB : resultado <objeto game>
GameDB -> GameREST: resultado <objeto game>
GameREST -> GameDB : executa movimento <pos> no <game object>\n<game object>.move(<pos>)
GameDB -> Database : salva movimento <game>.save()
Database -> GameDB : resultado ok
GameDB -> GameREST : resultado <objeto board>
GameREST -> Frontend : RESPONSE 200\n{ "response": <objeto board> }
Frontend -> Jogador : movimento realizado\ncom sucesso 
@enduml
```