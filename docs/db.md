## Modules

<dl>
<dt><a href="#module_db/utils">db/utils</a></dt>
<dd><p>useful functions</p>
</dd>
</dl>

## Classes

<dl>
<dt><a href="#Game">Game</a> ⇐ <code>moongose/Model</code></dt>
<dd><p>db/Game class: models Game operations with persistence</p>
</dd>
<dt><a href="#Player">Player</a> ⇐ <code>moongose/Model</code></dt>
<dd><p>db/Player class: models Player operations with persistence</p>
</dd>
<dt><a href="#Session">Session</a> ⇐ <code>moongose/Model</code></dt>
<dd><p>db/Session class: models Session operations with persistence</p>
</dd>
</dl>

<a name="module_db/utils"></a>

## db/utils
useful functions


* [db/utils](#module_db/utils)
    * _static_
        * [.listdb](#module_db/utils.listdb)
    * _inner_
        * [~listdb(a)](#module_db/utils..listdb)

<a name="module_db/utils.listdb"></a>

### db/utils.listdb
exports listdb function

**Kind**: static property of [<code>db/utils</code>](#module_db/utils)  
<a name="module_db/utils..listdb"></a>

### db/utils~listdb(a)
list objects in database at console

**Kind**: inner method of [<code>db/utils</code>](#module_db/utils)  

| Param | Type | Description |
| --- | --- | --- |
| a | <code>mongoose/Model</code> | mongoose Model |

**Example**  
```js
// list objects of Game collection
listdb(Game) 
```
<a name="Game"></a>

## Game ⇐ <code>moongose/Model</code>
db/Game class: models Game operations with persistence

**Kind**: global class  
**Extends**: <code>moongose/Model</code>, <code>models/Game</code>  
**See**

- [mongoose/Model](https://mongoosejs.com/docs/api/model.html)
- [models/Game](models.md#Game)

<a name="Player"></a>

## Player ⇐ <code>moongose/Model</code>
db/Player class: models Player operations with persistence

**Kind**: global class  
**Extends**: <code>moongose/Model</code>, <code>models/Player</code>  
**See**

- [mongoose/Model](https://mongoosejs.com/docs/api/model.html)
- [models/Player](models.md#Player)

<a name="Session"></a>

## Session ⇐ <code>moongose/Model</code>
db/Session class: models Session operations with persistence

**Kind**: global class  
**Extends**: <code>moongose/Model</code>, <code>models/Session</code>  
**See**

- [mongoose/Model](https://mongoosejs.com/docs/api/model.html)
- [models/Session](models.md#Session)

<a name="Session.open"></a>

### Session.open(login, pwd, cb)
Open a session

**Kind**: static method of [<code>Session</code>](#Session)  

| Param | Type | Description |
| --- | --- | --- |
| login | <code>string</code> | username |
| pwd | <code>string</code> | clear password |
| cb | <code>function</code> | callback function, with arguments: error and session opened |

