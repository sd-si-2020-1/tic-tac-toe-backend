## Modules

<dl>
<dt><a href="#module_models/errors">models/errors</a></dt>
<dd><p>Errors from models module</p>
</dd>
<dt><a href="#module_models">models</a></dt>
<dd><p>Models module</p>
</dd>
</dl>

## Classes

<dl>
<dt><a href="#Game">Game</a></dt>
<dd><p>Class representing tictactoe game</p>
</dd>
<dt><a href="#Player">Player</a></dt>
<dd><p>Class representing a tictactoe player</p>
</dd>
<dt><a href="#Room">Room</a></dt>
<dd><p>Class representing a game room</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#Session">Session()</a></dt>
<dd><p>Class representing player session</p>
</dd>
</dl>

<a name="module_models/errors"></a>

## models/errors
Errors from models module


* [models/errors](#module_models/errors)
    * [~SessionAlreadyOpenedError([message])](#module_models/errors..SessionAlreadyOpenedError)
    * [~SessionAlreadyClosedError([message])](#module_models/errors..SessionAlreadyClosedError)
    * [~SessionNotOpenedError([message])](#module_models/errors..SessionNotOpenedError)

<a name="module_models/errors..SessionAlreadyOpenedError"></a>

### models/errors~SessionAlreadyOpenedError([message])
Session already open error

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models/errors..SessionAlreadyClosedError"></a>

### models/errors~SessionAlreadyClosedError([message])
Session already closed error

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models/errors..SessionNotOpenedError"></a>

### models/errors~SessionNotOpenedError([message])
Session not opened

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models"></a>

## models
Models module


* [models](#module_models)
    * [.Game](#module_models.Game)
    * [.tictactoe](#module_models.tictactoe)
    * [.Player](#module_models.Player)
    * [.Room](#module_models.Room)
    * [.Session](#module_models.Session)

<a name="module_models.Game"></a>

### models.Game
export models/Game class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.tictactoe"></a>

### models.tictactoe
export models/tictactoe object

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.Player"></a>

### models.Player
export models/Player class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.Room"></a>

### models.Room
export models/Room class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.Session"></a>

### models.Session
export models/Session class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="Game"></a>

## Game
Class representing tictactoe game

**Kind**: global class  

* [Game](#Game)
    * [.move(pos)](#Game+move)
    * [.calculateWinner()](#Game+calculateWinner) ⇒ <code>string</code>

<a name="Game+move"></a>

### game.move(pos)
performs a movement on the board

**Kind**: instance method of [<code>Game</code>](#Game)  

| Param | Type | Description |
| --- | --- | --- |
| pos | <code>integer</code> | the position to perform the movement |

<a name="Game+calculateWinner"></a>

### game.calculateWinner() ⇒ <code>string</code>
calculates the current winner

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: <code>string</code> - - the winner ('X' or 'O') or null if there is no winner  
<a name="Player"></a>

## Player
Class representing a tictactoe player

**Kind**: global class  
<a name="Room"></a>

## Room
Class representing a game room

**Kind**: global class  
<a name="Session"></a>

## Session()
Class representing player session

**Kind**: global function  

* [Session()](#Session)
    * _instance_
        * [.close()](#Session+close)
    * _static_
        * [.logins](#Session.logins)
        * [.open(login, pwd)](#Session.open) ⇒ <code>object</code>
        * [.getPlayers()](#Session.getPlayers) ⇒ <code>array</code>
        * [.close(login)](#Session.close)

<a name="Session+close"></a>

### session.close()
Close session

**Kind**: instance method of [<code>Session</code>](#Session)  
**Throws**:

- session already closed
- session not opened

<a name="Session.logins"></a>

### Session.logins
List of player logins

**Kind**: static property of [<code>Session</code>](#Session)  
<a name="Session.open"></a>

### Session.open(login, pwd) ⇒ <code>object</code>
Open session

**Kind**: static method of [<code>Session</code>](#Session)  
**Returns**: <code>object</code> - session - if session sucessfully opened  
**Throws**:

- <code>SessionAlreadyOpened</code> - if a player try to open another session


| Param | Type |
| --- | --- |
| login | <code>string</code> | 
| pwd | <code>string</code> | 

<a name="Session.getPlayers"></a>

### Session.getPlayers() ⇒ <code>array</code>
Open session

**Kind**: static method of [<code>Session</code>](#Session)  
**Returns**: <code>array</code> - logins - list of player logins  
<a name="Session.close"></a>

### Session.close(login)
Open session

**Kind**: static method of [<code>Session</code>](#Session)  
**Throws**:

- <code>SessionAlreadyClosedError</code> - if the session is already closed


| Param | Type | Description |
| --- | --- | --- |
| login | <code>string</code> | login of player to be closed |

