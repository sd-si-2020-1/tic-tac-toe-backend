# Casos de uso

Veja também: [Manual do desenvolvedor](README.md)

## Introdução
* Tutorial: https://edisciplinas.usp.br/pluginfile.php/3720765/course/section/857581/Aula02_CasosDeUso.pdf
* Formatos: https://pt.wikipedia.org/wiki/Hist%C3%B3ria_de_usu%C3%A1rio#Formato
* Modelo com pré-condições, pós-condições, fluxo normal e alternativo: http://www.macoratti.net/11/10/uml_rev1.htm
* Plantuml docs: https://plantuml.com/use-case-diagram

## Diagrama de caso de uso: Papéis

```plantuml
@startuml papeis
left to right direction

actor "Visitante" as visitante
note top of visitante : qualquer um que\nacessa o sistema 

actor "Usuario" as usuario
note top of usuario : usuário é um visitante\n que efetuou login

actor "Jogador" as jogador
note top of jogador : jogador é um usuário que \nestá jogando em uma sala

rectangle Backend {
  usecase "uc1 - cadastrar conta" as uc1
  usecase "uc2 - abrir sessão" as uc2
  usecase "uc3 - criar uma sala" as uc3
  usecase "uc4 - listar salas" as uc4
  usecase "uc5 - entrar em uma sala" as uc5
  usecase "uc6 - realizar movimento" as uc6
}

usuario <|-- jogador
visitante <|-- usuario

visitante --> uc1
visitante --> uc2
usuario --> uc3
usuario --> uc4
usuario --> uc5
jogador --> uc6

@enduml
```

## Casos de uso textuais

| # | Título          | Descrição                                             | Pré-condições                              | Fluxo normal                                                | Fluxo alternativo                                                                                                                                 |
|---|-----------------|-------------------------------------------------------|--------------------------------------------|-------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | Cadastrar conta | como visitante,<br> gostaria de cadastrar minha conta | não há                                     | - visitante fornece login e senha<br> - cadastro é efetuado | - caso 'login já exista':<br>  notifica e solicita outro                                                                                          |
| 2 | Abrir sessão    | como visitante,<br> gostaria de abrir uma sessão      | visitante já efetou cadastro anteriormente | - visitante fornece login e senha<br> - a sessão é aberta   | - caso 'senha errada':<br> notifica e pede para informar novamente<br> - caso 'sessão já aberta':<br> notifica que somente uma sessão é permitida |
| 4| listar salas| A fim de buscar um jogo, como usuário, listo salas| usuário possui sessão aberta| - salas são listadas com propriedades:<br> id, status (opened, waiting_oponent, closed) | não há |
| 5| entrar em uma sala| Como usuário, entro em um sala| usuário possui sessão aberta| - usuário entra na sala<br>- muda status para jogador,<br>- oponente é notificado<br>- usuário recebe mensagem de sucesso<br>- sala muda para status 'playing'| - caso 'sala lotada ou fechada':<br>- usuário é notificado do problema |
| 6| realizar movimento| como jogador, realizo um movimento no tabuleiro| jogador se encontra em uma sala com status 'playing'| - jogador fornece a posição da jogada<br>- jogador recebe o tabuleiro atualizado com a jogada| - caso 'posição inválida ou jogo terminado': <br>- usuário é notificado do problema|