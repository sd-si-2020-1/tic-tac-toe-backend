var { Game } = require('../db/game.js');

/** 
 * @swagger
 * tags:
 *  name: Game
 *  description: operations with Tictactoe Games
 */

// OAS 3.0 uses 'components', OAS 2.0 uses 'definitions'
// see: https://swagger.io/docs/specification/components/
/**
 * @swagger
 * definitions:
 *    Game:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          description: the auto-incremented id of the game
 *        status:
 *          type: string
 *          description: game status, can ben 'opened' or 'closed'
 *        winner:
 *          type: string
 *          description: game winner, can be 'X', 'O' or null (without winner)
 *        next:
 *          type: string
 *          description: next player to move, can be 'X' or 'O'. 'X' is the first to move.
 *        board:
 *          type: array
 *          items:
 *            type: string
 *          minItems: 9
 *          maxItemns: 9
 *          description: board game, a array with 9 positions, from 0 to 8. 
*/

function int_id(req) {
  return parseInt(req.params.id)
}

function board(req, res, next) {
  Game.findById(int_id(req)).exec()
    .then((game)=>{res.send(game.board)})
    .catch((err)=>{next(err)})
	next();
}

function status(req, res, next) {
  Game.findById(int_id(req)).exec()
    .then(
      (game)=>{
        res.send({status: game.status})
        next();
      }
    )
    .catch((err)=>{next(err)})
}

function winner(req, res, next) {
  Game.findById(int_id(req)).exec()
    .then((game)=>{
      res.send({winner: game.winner})}
    )
    .catch((err)=>{next(err)})
	next();
}

function move(req, res, next) {
  const id = int_id(req)
  const pos = parseInt(req.params.pos)
  Game.findById(id).exec()
    .then(
      (game)=>{
        try {
          game.move(pos)
          game.markModified('board')
          game.save().then(
            ()=>{
              res.send(game.board)
              next();    
            }
          )
        } catch(err) {
          next(err)
        }
      }
    )
    .catch((err)=>{
      next(err)}
    )
}

function create(req, res, next) {
  let game = new Game()
  game.save().then(
    ()=>{
      res.send(game)
      next();
    }
  ).catch(
    (err)=>{next(err)}
  )
}

function setup(server) {
/**
 * @swagger
 *
 * /game/create:
 *   get:
 *     tags:
 *       - Game
 *     description: creates a new game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: the new game created
 */
server.get('/game/create', create);

/**
 * @swagger
 *
 * /game/{id}/board:
 *   get:
 *     tags:
 *       - Game
 *     description: returns the board of the game identified by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer 
 *     responses:
 *       200:
 *         description: current board game
 */
server.get('/game/:id/board', board);

/**
 * @swagger
 *
 * /game/{id}/move/{pos}:
 *   get:
 *     tags:
 *       - Game
 *     description: make a player movement
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer
 *       - name: pos
 *         in: path
 *         description: position 0..8 on board game
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: board game
 */
server.get('/game/:id/move/:pos', move);

/**
 * @swagger
 *
 * /game/{id}/status:
 *   get:
 *     description: returns the game status
 *     tags:
 *       - Game
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer 
 *     responses:
 *       200:
 *         description: current game status
 */
server.get('/game/:id/status', status);

/**
 * @swagger
 *
 * /game/{id}/winner:
 *   get:
 *     tags:
 *       - Game
 *     description: returns the game winner
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer 
 *     responses:
 *       200:
 *         description: game winner
 */
server.get('/game/:id/winner', winner);
}

module.exports = setup;