/**
 * Errors from rest module
 * @module rest/errors
 */

var errors = require('restify-errors');

const wrapError = (error) => {
  return  new errors.InternalServerError({
    code: error.name 
  }, error.message);
} 

module.exports.wrapError = wrapError