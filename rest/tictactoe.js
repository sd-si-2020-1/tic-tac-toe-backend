const { PositionAlreadyMarkedError, 
        GameClosedError } = require('../models/errors.js');
let { tictactoe } = require('../models/game.js');
let { wrapError } = require('./errors'); 

/** 
 * @swagger
 * tags:
 *  name: Tictactoe
 *  description: operations with a tictactoe object
*/

function board(req, res, next) {
	res.send(tictactoe.board);
	next();
}

function init(req, res, next) {
  tictactoe.init()
	res.send(tictactoe.board);
	next();
}

function status(req, res, next) { 
  res.send(tictactoe.status)
	next();
}

function status_all(req, res, next) { 
  const { status, winner } = tictactoe;
  res.send({status, winner, next: tictactoe.next })
	next();
}

function winner(req, res, next) {
	res.send(tictactoe.winner);
	next();
}

function next_player(req, res, next) {
	res.send(tictactoe.next);
	next();
}

function move(req, res, next) {
  var pos = req.params.pos
  const pos_int = parseInt(pos)
	try {
		tictactoe.move(pos_int)
	} catch(e) {
    if (e instanceof PositionAlreadyMarkedError ||
        e instanceof GameClosedError
      ) {
      return next(wrapError(e))
    }
    else
      return next(e)
	}
	res.send(tictactoe.board);
	next();
}

function setup(server) {
/**
 * @swagger
 *
 * /tictactoe/board:
 *   get:
 *     tags:
 *       - Tictactoe
 *     description: returns the board game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current board game
 */
server.get('/tictactoe/board', board);

/**
 * @swagger
 *
 * /tictactoe/move/{pos}:
 *   get:
 *     tags:
 *       - Tictactoe
 *     description: make a player movement
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: pos
 *         in: path
 *         description: position 0..8 on board game
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: board game
 */
server.get('/tictactoe/move/:pos', move);

/**
 * @swagger
 *
 * /tictactoe/status:
 *   get:
 *     description: returns the game status
 *     tags:
 *       - Tictactoe
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current game status
 */
server.get('/tictactoe/status', status);

/**
 * @swagger
 *
 * /tictactoe/status/all:
 *   get:
 *     description: returns the game status, next player and winner
 *     tags:
 *       - Tictactoe
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current game status, next player and winner
 */
server.get('/tictactoe/status/all', status_all);

/**
 * @swagger
 *
 * /tictactoe/winner:
 *   get:
 *     tags:
 *       - Tictactoe
 *     description: returns the game winner
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: game winner
 */
server.get('/tictactoe/winner', winner);

/**
 * @swagger
 *
 * /tictactoe/next:
 *   get:
 *     tags:
 *       - Tictactoe
 *     description: returns the next player to move
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: player 'X' or 'O'
 */
server.get('/tictactoe/next', next_player);

/**
 * @swagger
 *
 * /tictactoe/init:
 *   get:
 *     tags:
 *       - Tictactoe
 *     description: initialize (restart) the game 
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: return the current board
 */
server.get('/tictactoe/init', init);

}

module.exports = setup;