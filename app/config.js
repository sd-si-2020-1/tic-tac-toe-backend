const config = {
  db_host: 'localhost',
  db_name: 'tictactoe',
  rest_host: '127.0.0.1',
  rest_port: 8080,
  swagger_host: '127.0.0.1',
  swagger_port: 3000,
  secret: 'a%&\gH1.J%F"*TxM~d%>D(qje',
  cors_origins: ['*']
}

module.exports = config