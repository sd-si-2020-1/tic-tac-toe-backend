test('open only one session', () => {
    var {Session} = require('../../models/');
    var {SessionAlreadyOpenedError} = require('../../models/errors');
    var s1 = Session.open('teste1');
    expect(()=>{Session.open('teste1')}).toThrowError(SessionAlreadyOpenedError);
})