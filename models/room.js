const { Game } = require('./game')

/**
 * Class representing a game room
 */
class Room {
  players = [];
  game = null;
  enter(player) {
    if ( this.players.length == 2 )
      throw new Error('room is crowded')
    else
      this.players.push(player)
  }
	exit(player) {
    const index = this.players.indexOf(player)
    if ( index > -1 )
      this.players.splice(index,1)
    else
      throw new Error('player not found')
  }
}
module.exports.Room = Room