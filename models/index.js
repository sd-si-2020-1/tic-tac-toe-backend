/**
 * Models module
 * @module models
 */

const { Game, tictactoe } = require('./game')
const { Player } = require('./player')
const { Room } = require('./room')
const { Session } = require('./session')

/** export models/Game class */
module.exports.Game = Game

/** export models/tictactoe object */
module.exports.tictactoe = tictactoe

/** export models/Player class */
module.exports.Player = Player

/** export models/Room class */
module.exports.Room = Room

/** export models/Session class */
module.exports.Session = Session