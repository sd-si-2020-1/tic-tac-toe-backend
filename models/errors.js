/**
 * Errors from models module
 * @module models/errors
 */

 /**
 * Game closed error
 * @param {string} [message] custom message
 * @see https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error 
 */
function GameClosedError(message) {
  this.name = 'GameClosedError';
  this.message = message || 'The game is closed';
  this.stack = (new Error()).stack;
}

 /**
 * Position already marked error
 * @param {string} [message] custom message
 * @see https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error 
 */
function PositionAlreadyMarkedError(message) {
  this.name = 'PositionAlreadyMarkedError';
  this.message = message || 'Position is already marked';
  this.stack = (new Error()).stack;
}

/**
 * Session already open error
 * @param {string} [message] custom message
 * @see https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error 
 */
function SessionAlreadyOpenedError(message) {
  this.name = 'SessionAlreadyOpenedError';
  this.message = message || 'There is already a session opened';
  this.stack = (new Error()).stack;
}

/**
 * Session already closed error
 * @param {string} [message] custom message
 * @see https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error 
 */
function SessionAlreadyClosedError(message) {
  this.name = 'SessionAlreadyClosedError';
  this.message = message || 'Session is already closed';
  this.stack = (new Error()).stack;
}

/**
 * Session not opened
 * @param {string} [message] custom message
 * @see https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error 
 */
function SessionNotOpenedError(message) {
  this.name = 'SessionNotOpenedError';
  this.message = message || 'Session is not opened';
  this.stack = (new Error()).stack;
}

module.exports.SessionNotOpenedError = SessionNotOpenedError
module.exports.SessionAlreadyClosedError = SessionAlreadyClosedError
module.exports.SessionAlreadyOpenedError = SessionAlreadyOpenedError
module.exports.GameClosedError = GameClosedError
module.exports.PositionAlreadyMarkedError = PositionAlreadyMarkedError