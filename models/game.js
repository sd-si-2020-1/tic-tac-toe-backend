const { GameClosedError, PositionAlreadyMarkedError } = require('./errors')

/** 
 * Class representing tictactoe game
*/
class Game {
  constructor(board,status,next,count,winner) {
    this.init(board,status,next,count,winner)
  }
  init(board,status,next,count,winner) {
    this.board = board ? board: [
      null, null, null,
      null, null, null,
      null, null, null
    ];
    this.status = status ? status: 'opened';
    this.next = next ? next: 'X';
    this.count = count ? count: 0;
    this.winner = winner ? winner: null;  
  }
	board = [
		null,null,null,
		null,null,null,
		null,null,null
	];
	status = 'opened';
	next = 'X';
	count = 0;
  winner = null;

  /**
   * performs a movement on the board
   * @param {integer} pos - the position to perform the movement 
   */
  // TODO: document errors
	move(pos) {
		if ( this.status == 'closed' )
      throw new GameClosedError()
    if ( ! Number.isInteger(pos) || pos < 0 || pos > 8 )
      throw new Error('Invalid position')
		if ( this.board[pos] != null )
			throw new PositionAlreadyMarkedError()
		this.board[pos] = this.next;
		this.next = this.next == 'X' ? 'O' : 'X'
		this.winner = this.calculateWinner()
		this.count++
		if ( this.count == 9 || this.winner )
			this.status = 'closed'
  }
  
  /**
   * calculates the current winner
   * @returns {string} - the winner ('X' or 'O') or null if there is no winner
   */
	calculateWinner() {
  		const lines = [
    			[0, 1, 2],
    			[3, 4, 5],
    			[6, 7, 8],
    			[0, 3, 6],
    			[1, 4, 7],
    			[2, 5, 8],
    			[0, 4, 8],
			[2, 4, 6],
  		];
  		for (let i = 0; i < lines.length; i++) {
    			const [a, b, c] = lines[i];
    			if (
				this.board[a] && 
				this.board[a] === this.board[b] && 
				this.board[a] === this.board[c]) {
      				return this.board[a];
    			}
  		}
  		return null;
  }
}

tictactoe = new Game()
module.exports.tictactoe = tictactoe
module.exports.Game = Game