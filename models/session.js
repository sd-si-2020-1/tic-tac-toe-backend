const { SessionAlreadyOpenedError, 
        SessionAlreadyClosedError,
        SessionNotOpenedError } = require('./errors')

/** 
 * Class representing player session
*/
function Session() {
  this.player = null
  this.status = null
  this.start = null
  this.end = null
}

// instance methods: use prototype
// see: https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_prototypes

/**
 * Close session
 * @throws session already closed
 * @throws session not opened
 */
Session.prototype.close = function close() {
  if ( this.status == 'open') {
    Session.close(this.login)
    this.end = Date.now()
    this.status = 'close'      
  }
  else
    throw new SessionNotOpenedError()
}

/**
 * List of player logins
 * @static
 */
Session.logins = []

// static methods
/**
 * Open session
 * @param {string} login 
 * @param {string} pwd 
 * @returns {object} session - if session sucessfully opened
 * @throws {SessionAlreadyOpened} - if a player try to open another session
 */
Session.open = function open(login) {
  // pre-condition: player password already checked 
  if ( Session.logins.indexOf(login) > -1 )
    throw new SessionAlreadyOpenedError()
  var session = new Session()
  session.status = 'open'
  session.start = Date.now()
  session.login = login;
  Session.logins.push(login)
  return session;
}

/**
 * Open session
 * @returns {array} logins - list of player logins
 */
Session.getPlayers = function getPlayers() {
  return Session.logins;
}
/**
 * Open session
 * @param {string} login - login of player to be closed
 * @throws {SessionAlreadyClosedError} - if the session is already closed 
 */
Session.close = function close(login) {
  const index = Session.logins.indexOf(login)
  if ( index > -1 )
    Session.logins.splice(index,1)
  else
    throw new SessionAlreadyClosedError()        
}
 
module.exports.Session = Session