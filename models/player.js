const crypto = require('crypto');
const { encrypt } = require('../app/.')

/**
 * Class representing a tictactoe player
 */
class Player {
  login = null
  password = null
  
  // TODO: document Player methods
	change_pwd(pwd_txt) {
    this.password = encrypt(pwd_txt)
  }
	check_pwd(attempt) {
    return encrypt(attempt) === this.password ? true : false;
	}
}
module.exports.Player = Player