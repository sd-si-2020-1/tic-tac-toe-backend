const db = require('./database').connect();
module.exports.db = db

module.exports.Game = require('./game')
module.exports.Player = require('./player')
module.exports.Room = require('./room')
module.exports.Session = require('./session')

var { listdb } = require('./util')
module.exports.listdb = listdb