const db = require('./database').connect();
var { tictactoe } = require('../models/game')

const gameSchema = new db.Schema({
  game: { type: db.Schema.Types.ObjectId, ref: 'Game' },
  status: { type: String, default: tictactoe.status },
  next: { type: String, default: tictactoe.next },
  winner: { type: String, default: tictactoe.winner },
  board: { 
    type: Array, 
    default: tictactoe.board
  } 
});
gameSchema.plugin(db.autoIncrement.plugin, 'Game');
gameSchema.methods.move = tictactoe.move;
gameSchema.methods.calculateWinner = tictactoe.calculateWinner;


/**
 * @class Game
 * @classdesc db/Game class: models Game operations with persistence
 * @augments moongose/Model
 * @augments models/Game
 * @see {@link https://mongoosejs.com/docs/api/model.html|mongoose/Model}
 * @see {@link models.md#Game|models/Game}  
 */
module.exports = db.connection.model('Game', gameSchema);