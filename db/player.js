const db = require('./database').connect();
const { Player } = require('../models/')

player = new Player()

const playerSchema = new db.Schema({
  player: { type: db.Schema.Types.ObjectId, ref: 'Player' },
  login: { type: String, default: player.login },
  password: { type: String, default: player.password }
});
playerSchema.plugin(db.autoIncrement.plugin, 'Player');
playerSchema.methods.change_pwd = player.change_pwd;
playerSchema.methods.check_pwd = player.check_pwd;

playerSchema.statics.findByLogin = function findByLogin(login, cb) {
  return this.findOne({ login }).exec(cb)
}
playerSchema.statics.check_pwd = function check_pwd(login, pwd, cb) {
  this.findByLogin(login,(err,p)=>{
    if (p) {
      if ( ! p.check_pwd(pwd) )
        return cb(err, null);
    }
    return cb(err, p);   
  })
}

/**
 * @class Player
 * @classdesc db/Player class: models Player operations with persistence
 * @augments moongose/Model
 * @augments models/Player
 * @see {@link https://mongoosejs.com/docs/api/model.html|mongoose/Model}
 * @see {@link models.md#Player|models/Player}  
 */
module.exports = db.connection.model('Player', playerSchema);