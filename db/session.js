const db = require('./database').connect();
const { Session } = require('../models/');
const { Player } = require('./')

const sessionSchema = new db.Schema({
  session: { type: db.Schema.Types.ObjectId, ref: 'Session' },
  login: { type: String, default: null },
  status: { type: String, default: null },
  start: { type: Date, default: null },
  end: { type: Date, default: null },
});
sessionSchema.plugin(db.autoIncrement.plugin, 'Session');
sessionSchema.methods.close = Session.prototype.close;
sessionSchema.statics.getPlayers = Session.getPlayers;
sessionSchema.statics.close = Session.close;

/**
 * Open a session
 * @memberof Session
 * @function
 * @static
 * @name open
 * @param {string} login - username
 * @param {string} pwd - clear password 
 * @param {function} cb - callback function, with arguments: error and session opened
 */
sessionSchema.statics.open = function open(login, pwd, cb) {
  Player.check_pwd(login, pwd,
    (err,p)=>{
      if (p) {
        try {
          model_opened = Session.open(login);
          // this equivalent of 'db/Session'
          const sess_opened = new this(model_opened)     
          cb(null, sess_opened)
        } catch(err) {
          console.log("error: "+err)
          cb(err,null)
        }
      }
    });
}


/**
 * @class Session
 * @classdesc db/Session class: models Session operations with persistence
 * @augments moongose/Model
 * @augments models/Session
 * @see {@link https://mongoosejs.com/docs/api/model.html|mongoose/Model}
 * @see {@link models.md#Session|models/Session}  
 */
module.exports = db.connection.model('Session', sessionSchema)