const { db } = require('./')
const { Room } = require('../models/')

room = new Room()

const roomSchema = new db.Schema({
  room: { type: db.Schema.Types.ObjectId, ref: 'Room' },  
  players: [ { type: db.Schema.Types.ObjectId, ref: 'Player' } ],
  game: { type: db.Schema.Types.ObjectId, ref: 'Game' }
});
roomSchema.plugin(db.autoIncrement.plugin, 'Room');
roomSchema.methods.enter = room.enter;
roomSchema.methods.exit = room.exit;

module.exports = db.connection.model('Room', roomSchema);